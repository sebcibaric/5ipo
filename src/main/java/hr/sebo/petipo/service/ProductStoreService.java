package hr.sebo.petipo.service;

import java.util.List;

import hr.sebo.petipo.dto.ProductStoreDTO;
import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.model.ProductStore;

public interface ProductStoreService {

    ProductStoreDTO findById(Long id) throws ResourceNotFoundException;
    
    List<ProductStoreDTO> findAll() throws ResourceNotFoundException;

    List<ProductStoreDTO> findAllByStoreId(Integer storeId) throws ResourceNotFoundException;

    ProductStoreDTO saveProductStore(ProductStoreDTO productStoreDTO) throws ResourceSavingException;

    ProductStoreDTO editProductStore(ProductStoreDTO productStoreDTO) throws ResourceNotFoundException, ResourceSavingException;

    void deleteProductStoreById(Long id) throws ResourceDeletionException;
}