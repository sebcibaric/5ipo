package hr.sebo.petipo.service;

import java.util.List;

import hr.sebo.petipo.dto.RoleDTO;
import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.model.Role;

public interface RoleService {

    RoleDTO findById(Short id) throws ResourceNotFoundException;

    List<RoleDTO> findAll() throws ResourceNotFoundException;

    RoleDTO saveRole(RoleDTO roleDTO) throws ResourceSavingException;

    RoleDTO editRole(RoleDTO roleDTO) throws ResourceNotFoundException, ResourceSavingException;

    void deleteRoleById(Short id) throws ResourceDeletionException;
}