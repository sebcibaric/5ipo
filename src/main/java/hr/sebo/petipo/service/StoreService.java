package hr.sebo.petipo.service;

import hr.sebo.petipo.dto.StoreDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.model.Store;

import java.util.List;

public interface StoreService {

    StoreDTO findById(Integer id) throws ResourceNotFoundException;
    
    List<StoreDTO> findAll() throws ResourceNotFoundException;

    StoreDTO saveStore(StoreDTO storeDTO) throws ResourceSavingException;

    StoreDTO editStore(StoreDTO storeDTO) throws ResourceNotFoundException, ResourceSavingException;

    void deleteStoreById(Integer id) throws ResourceDeletionException;
}