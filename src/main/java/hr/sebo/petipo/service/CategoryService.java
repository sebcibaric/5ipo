package hr.sebo.petipo.service;

import java.util.List;

import hr.sebo.petipo.dto.CategoryDTO;
import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;

public interface CategoryService {

    CategoryDTO findById(Integer id) throws ResourceNotFoundException;

    List<CategoryDTO> findAll() throws ResourceNotFoundException;

    CategoryDTO saveCategory(CategoryDTO categoryDTO) throws ResourceSavingException;

    CategoryDTO editCategory(CategoryDTO categoryDTO) throws ResourceSavingException, ResourceNotFoundException;

    void deleteCategoryById(Integer id) throws ResourceDeletionException;
}