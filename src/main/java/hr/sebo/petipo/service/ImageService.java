package hr.sebo.petipo.service;

import hr.sebo.petipo.enums.ImageType;

public interface ImageService {
    String createImagePath(String name, ImageType imageType);

    void saveImage(byte[] bytes, String path);

    byte[] loadImageAsByteArray(String path);

    String loadImageAsBase64(String path);

    boolean deleteImage(String path);
}
