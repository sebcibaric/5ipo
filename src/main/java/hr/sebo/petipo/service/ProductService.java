package hr.sebo.petipo.service;

import java.util.List;

import hr.sebo.petipo.dto.ProductDTO;
import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.model.Product;

public interface ProductService {

    ProductDTO findById(Long id) throws ResourceNotFoundException;
    
    List<ProductDTO> findAll() throws ResourceNotFoundException;

    ProductDTO saveProduct(ProductDTO productDTO) throws ResourceSavingException;

    ProductDTO editProduct(ProductDTO productDTO) throws ResourceNotFoundException, ResourceSavingException;

    void deleteProductById(Long id) throws ResourceNotFoundException, ResourceDeletionException;
}