package hr.sebo.petipo.service;

import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.UserResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    UserDTO findById(Short id) throws ResourceNotFoundException;

    UserDTO findByUsername(String username) throws ResourceNotFoundException;

    List<UserDTO> findAll() throws ResourceNotFoundException;

    UserDTO saveUser(UserDTO userDTO) throws ResourceSavingException;

    UserDTO editUser(UserDTO userDTO) throws ResourceNotFoundException, ResourceSavingException;

    void deleteUserById(Short id) throws ResourceDeletionException;

    UserResponse signUp(UserDTO userDTO);

    UserResponse authenticate(UserDTO userDTO);

    String findPasswordById(Short id) throws ResourceNotFoundException;

    void resetUserPassword(Short id) throws ResourceNotFoundException;
}