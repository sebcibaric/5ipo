package hr.sebo.petipo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.sebo.petipo.model.Role;

public interface RoleRepository extends JpaRepository<Role, Short> {

}