package hr.sebo.petipo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.sebo.petipo.model.ProductStore;

import java.util.List;

public interface ProductStoreRepository extends JpaRepository<ProductStore, Long> {

    List<ProductStore> findAllByStore_Id(Integer storeId);
}