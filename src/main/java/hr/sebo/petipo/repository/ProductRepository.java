package hr.sebo.petipo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.sebo.petipo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}