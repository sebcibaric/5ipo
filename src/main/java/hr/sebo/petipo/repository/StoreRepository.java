package hr.sebo.petipo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.sebo.petipo.model.Store;

public interface StoreRepository extends JpaRepository<Store, Integer> {

}