package hr.sebo.petipo.repository;

import hr.sebo.petipo.exceptions.BusinessException;
import org.springframework.data.jpa.repository.JpaRepository;

import hr.sebo.petipo.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Short> {
    @Query("SELECT user from User user " +
            "join fetch user.role " +
            "left join fetch user.store " +
            "where user.username = :username "
    )
    Optional<User> findByUsername(@Param("username") String username);
}