package hr.sebo.petipo.configuration;

public class Constants {
    public static final String SECRET = "sebosebo";
    public static final long EXPIRATION_TIME = 864000000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/users/sign-up";
    public static final String LOG_IN_URL = "/api/users/login";
}