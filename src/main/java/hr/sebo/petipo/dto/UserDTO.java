package hr.sebo.petipo.dto;

import hr.sebo.petipo.info.AdvanceInfo;
import hr.sebo.petipo.info.BasicInfo;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class UserDTO {

    private Short id;

    @NotEmpty(message = "Username must not be empty", groups = { BasicInfo.class, AdvanceInfo.class})
    private String username;

    @NotEmpty(message = "Password must not be empty", groups = { BasicInfo.class, AdvanceInfo.class})
    private String password;

    @NotEmpty(message = "Frist name must not be empty", groups = AdvanceInfo.class)
    private String firstName;

    @NotEmpty(message = "Last name must not be empty", groups = AdvanceInfo.class)
    private String lastName;

    private Byte storeId;

    private String storeName;

    @NotEmpty(message = "Role must not be empty", groups = AdvanceInfo.class)
    @Min(value = 0, message = "roleId must not be lower than 0", groups = AdvanceInfo.class)
    private Byte roleId;

    private String roleName;

}
