package hr.sebo.petipo.dto;

import lombok.Data;

@Data
public class ProductStoreDTO {

    private Long id;

    private Long productId;

    private String productName;

    private Byte storeId;

    private String storeName;

    private Integer categoryId;

    private String categoryName;

    private double price;
}
