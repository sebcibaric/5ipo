package hr.sebo.petipo.dto;

import lombok.Data;

@Data
public class CategoryDTO {

    private Integer id;

    private String name;

}
