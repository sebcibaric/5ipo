package hr.sebo.petipo.dto;

import lombok.Data;

@Data
public class StoreDTO {

    private Integer id;

    private String name;

}
