package hr.sebo.petipo.dto;

import lombok.Data;

@Data
public class RoleDTO {

    private Short id;

    private String name;

}
