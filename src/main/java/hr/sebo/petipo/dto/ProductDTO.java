package hr.sebo.petipo.dto;

import lombok.Data;

@Data
public class ProductDTO {

    private Long id;

    private String name;

    private String imagePath;

    private Integer categoryId;

    private String categoryName;

    private String base64;
}
