package hr.sebo.petipo.controller;

import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Response> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult().getAllErrors().stream()
                .map(this::getErrorMessage)
                .collect(Collectors.toList());

        return ResponseEntity
                .badRequest()
                .body(new Response(HttpStatus.BAD_REQUEST, "Bad request", errors));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Response> handleExceptions(IllegalArgumentException ex) {
        return ResponseEntity
                .badRequest()
                .body(new Response(HttpStatus.BAD_REQUEST, ex.getMessage()));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Response> handleExceptions(ResourceNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Response(HttpStatus.NOT_FOUND, ex.getMessage()));
    }

    @ExceptionHandler(ResourceSavingException.class)
    public ResponseEntity<Response> handleExceptions(ResourceSavingException ex) {
        return ResponseEntity
                .badRequest()
                .body(new Response(HttpStatus.BAD_REQUEST, ex.getMessage()));
    }

    @ExceptionHandler(ResourceDeletionException.class)
    public ResponseEntity<Response> handleExceptions(ResourceDeletionException ex) {
        return ResponseEntity
                .badRequest()
                .body(new Response(HttpStatus.BAD_REQUEST, ex.getMessage()));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Response> handleExceptions(BadCredentialsException ex) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new Response(HttpStatus.UNAUTHORIZED, ex.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> handleExceptions(Exception ex) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new Response(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()));
    }

    public String getErrorMessage(ObjectError error) {
        return error.getDefaultMessage();
    }
}
