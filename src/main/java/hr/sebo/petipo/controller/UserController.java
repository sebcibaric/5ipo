package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.dto.StoreDTO;
import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.info.AdvanceInfo;
import hr.sebo.petipo.info.BasicInfo;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.pojo.UserResponse;
import hr.sebo.petipo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/login")
    public ResponseEntity<Response> login(@RequestBody @Validated(BasicInfo.class) UserDTO userDTO) {
        log.info("User {} is logging in", userDTO.getUsername());

        UserResponse userResponse = service.authenticate(userDTO);

        log.info("User {} logged in successfully", userDTO.getUsername());
        return ResponseEntity.ok(new Response(HttpStatus.OK, "User logged in", userResponse));
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Response> signUp(@RequestBody @Validated(AdvanceInfo.class) UserDTO user) {
        log.info("User {} signing up", user.getUsername());

        UserResponse userResponse = service.signUp(user);

        log.info("User {} signed up successfully", user.getUsername());
        return ResponseEntity.ok(new Response(HttpStatus.OK, "User signed up", userResponse));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Short id) throws ResourceNotFoundException {
        UserDTO userDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, userDTO));
    }

    @GetMapping("/name/{username}")
    public ResponseEntity<Response> findByUsername(@PathVariable String username) throws ResourceNotFoundException {
        UserDTO userDTO = service.findByUsername(username);

        return ResponseEntity.ok(new Response(HttpStatus.OK, userDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<UserDTO> users = service.findAll();

        return ResponseEntity.ok(new Response(HttpStatus.OK, users));
    }

    @PostMapping
    public ResponseEntity<Response> saveUser(@RequestBody UserDTO userDTO) throws ResourceSavingException {
        userDTO = service.saveUser(userDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Korisnik spremljen", userDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editUser(@RequestBody UserDTO userDTO) throws ResourceNotFoundException, ResourceSavingException {
        userDTO = service.editUser(userDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Korisnik ažuriran", userDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteUserById(@PathVariable Short id) throws ResourceNotFoundException, ResourceDeletionException {
        service.deleteUserById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Korisnik obrisan"));
    }

    @PutMapping("/{id}/password")
    public ResponseEntity<Response> resetUserPassword(@PathVariable Short id) throws ResourceNotFoundException, ResourceSavingException {
        service.resetUserPassword(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Korisniku resetirana lozinka"));
    }
}