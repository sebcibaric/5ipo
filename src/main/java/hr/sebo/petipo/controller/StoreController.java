package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.StoreDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/stores")
public class StoreController {

    @Autowired
    private StoreService service;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        StoreDTO storeDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, storeDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<StoreDTO> stores = service.findAll();

        return ResponseEntity.ok(new Response(HttpStatus.OK, stores));
    }

    @PostMapping
    public ResponseEntity<Response> saveStore(@RequestBody StoreDTO storeDTO) throws ResourceSavingException {
        storeDTO = service.saveStore(storeDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Proizvod spremljen", storeDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editStore(@RequestBody StoreDTO storeDTO) throws ResourceNotFoundException, ResourceSavingException {
        storeDTO = service.editStore(storeDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod ažuriran", storeDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteStoreById(@PathVariable Integer id) throws ResourceNotFoundException, ResourceDeletionException {
        service.deleteStoreById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod obrisan"));
    }
}