package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.ProductStoreDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.service.ProductStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/productStores")
public class ProductStoreController {

    @Autowired
    private ProductStoreService service;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Long id) throws ResourceNotFoundException {
        ProductStoreDTO productStoreDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, productStoreDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<ProductStoreDTO> productStores = service.findAll();

        return ResponseEntity.ok(new Response(HttpStatus.OK, productStores));
    }

    @GetMapping("/store/{storeId}")
    public ResponseEntity<Response> findByStoreId(@PathVariable Integer storeId) throws ResourceNotFoundException {
        List<ProductStoreDTO> productStores = service.findAllByStoreId(storeId);

        return ResponseEntity.ok(new Response(HttpStatus.OK, productStores));
    }

    @PostMapping
    public ResponseEntity<Response> saveProductStore(@RequestBody ProductStoreDTO productStoreDTO) throws ResourceSavingException {
        productStoreDTO = service.saveProductStore(productStoreDTO);

        return ResponseEntity
                    .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Proizvod spremljen", productStoreDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editProductStore(@RequestBody ProductStoreDTO productStoreDTO) throws ResourceNotFoundException, ResourceSavingException {
        productStoreDTO = service.editProductStore(productStoreDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod ažuriran", productStoreDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteProductStoreById(@PathVariable Long id) throws ResourceNotFoundException, ResourceDeletionException {
        service.deleteProductStoreById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod obrisan"));
    }
}