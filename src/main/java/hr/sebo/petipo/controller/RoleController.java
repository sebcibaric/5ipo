package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.RoleDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/roles")
public class RoleController {

    @Autowired
    private RoleService service;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Short id) throws ResourceNotFoundException {
        RoleDTO roleDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, roleDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<RoleDTO> roles = service.findAll();

        return ResponseEntity.ok(new Response(HttpStatus.OK, roles));
    }

    @PostMapping
    public ResponseEntity<Response> saveRole(@RequestBody RoleDTO roleDTO) throws ResourceSavingException {
        roleDTO = service.saveRole(roleDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Proizvod spremljen", roleDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editRole(@RequestBody RoleDTO roleDTO) throws ResourceNotFoundException, ResourceSavingException {
        roleDTO = service.editRole(roleDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod ažuriran", roleDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteRoleById(@PathVariable Short id) throws ResourceNotFoundException, ResourceDeletionException {
        service.deleteRoleById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod obrisan"));
    }
}