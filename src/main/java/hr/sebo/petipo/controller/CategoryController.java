package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.CategoryDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        CategoryDTO categoryDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, categoryDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<CategoryDTO> categories = service.findAll();
        
        return ResponseEntity.ok(new Response(HttpStatus.OK, categories));
    }

    @PostMapping
    public ResponseEntity<Response> saveCategory(@RequestBody CategoryDTO categoryDTO) throws ResourceSavingException {
        categoryDTO = service.saveCategory(categoryDTO);
    
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Kategorija spremljena", categoryDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editCategory(@RequestBody CategoryDTO categoryDTO) throws ResourceNotFoundException, ResourceSavingException {
        categoryDTO = service.editCategory(categoryDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Kategorija ažurirana", categoryDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteCategoryById(@PathVariable Integer id) throws ResourceDeletionException {
        service.deleteCategoryById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Kategorija obrisana"));
    }
}