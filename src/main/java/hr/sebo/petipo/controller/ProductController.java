package hr.sebo.petipo.controller;

import hr.sebo.petipo.dto.ProductDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.pojo.Response;
import hr.sebo.petipo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping("/{id}")
    public ResponseEntity<Response> findById(@PathVariable Long id) throws ResourceNotFoundException {
        ProductDTO productDTO = service.findById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, productDTO));
    }

    @GetMapping
    public ResponseEntity<Response> findAll() throws ResourceNotFoundException {
        List<ProductDTO> categories = service.findAll();

        return ResponseEntity.ok(new Response(HttpStatus.OK, categories));
    }

    @PostMapping
    public ResponseEntity<Response> saveProduct(@RequestBody ProductDTO productDTO) throws ResourceSavingException {
        productDTO = service.saveProduct(productDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new Response(HttpStatus.CREATED, "Proizvod spremljen", productDTO));
    }

    @PutMapping
    public ResponseEntity<Response> editProduct(@RequestBody ProductDTO productDTO) throws ResourceNotFoundException, ResourceSavingException {
        productDTO = service.editProduct(productDTO);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod ažuriran", productDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteProductById(@PathVariable Long id) throws ResourceNotFoundException, ResourceDeletionException {
        service.deleteProductById(id);

        return ResponseEntity.ok(new Response(HttpStatus.OK, "Proizvod obrisan"));
    }
}