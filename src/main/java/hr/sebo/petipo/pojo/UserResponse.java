package hr.sebo.petipo.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

@Setter
@Getter
public class UserResponse extends User {

    private Short id;

    private String firstName;

    private String lastName;

    private Short roleId;

    private String roleName;

    private Integer storeId;

    private String storeName;

    private String token;

    public UserResponse(Short id, String username, String password, String firstName, String lastName, Short roleId,
                        String roleName, Integer storeId, String storeName, String token) {
        super(username, password, new ArrayList<>());
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.storeId = storeId;
        this.storeName = storeName;
        this.token = token;
    }
}