package hr.sebo.petipo.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Response {

    public Response() {}

    public Response(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response(HttpStatus status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Response(HttpStatus status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Response(HttpStatus status, String message, List<String> errors) {
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    private HttpStatus status;

    private String message;

    private Object data;

    private List<String> errors;
}
