package hr.sebo.petipo.implementation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import hr.sebo.petipo.dto.RoleDTO;
import hr.sebo.petipo.dto.RoleDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.RoleMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.model.Role;
import hr.sebo.petipo.repository.RoleRepository;
import hr.sebo.petipo.service.RoleService;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository repository;

    @Autowired
    private RoleMapper mapper;

    @Override
    public RoleDTO findById(@NotNull Short id) throws ResourceNotFoundException {
        log.info("findById id: {} started ", id);

        RoleDTO roleDTO = repository.findById(id)
                .map(mapper::roleToRoleDTO)
                .orElseThrow(() -> {
                    log.warn("Role not found by id: {}", id);
                    return new ResourceNotFoundException("Proizvod nije pronađen");
                });

        log.info("role found by id: {}", id);
        return roleDTO;
    }

    @Override
    public List<RoleDTO> findAll() throws ResourceNotFoundException {
        log.info("Role findAll started");

        List<RoleDTO> roles = repository.findAll()
                .stream()
                .map(mapper::roleToRoleDTO)
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(roles)) {
            log.info("Roles not found");
            throw new ResourceNotFoundException("Role nisu pronađeni");
        }

        log.info("Role findAll finished");
        return roles;
    }

    @Override
    public RoleDTO saveRole(@NotNull RoleDTO roleDTO) throws ResourceSavingException {
        log.info("Role saving started with id: {}", roleDTO.getId());

        try {
            Role role = repository.save(mapper.roleDTOToRole(roleDTO));
            roleDTO = mapper.roleToRoleDTO(role);
        } catch (Exception e) {
            throw new ResourceSavingException("Spremanje role nije uspjelo");
        }

        log.info("Role saving successfully finished");
        return roleDTO;
    }

    @Override    
    public RoleDTO editRole(@NotNull RoleDTO roleDTO) throws ResourceNotFoundException, ResourceSavingException {
        log.info("Role editing started with id: {}", roleDTO.getId());

        findById(roleDTO.getId());

        try {
            Role role = repository.save(mapper.roleDTOToRole(roleDTO));
            roleDTO = mapper.roleToRoleDTO(role);
        } catch (Exception e) {
            throw new ResourceSavingException("Ažuriranje role nije uspjelo");
        }

        log.info("Role editing successfully finsihed");
        return roleDTO;
    }


    @Override
    public void deleteRoleById(@NotNull Short id) throws ResourceDeletionException {
        log.info("deleteRoleById id: {} started", id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResourceDeletionException("Brisanje role nije uspjelo");
        }

        log.info("Role with id: {} succesfully deleted", id);
    }
}