package hr.sebo.petipo.implementation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import hr.sebo.petipo.dto.StoreDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.StoreMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.model.Store;
import hr.sebo.petipo.repository.StoreRepository;
import hr.sebo.petipo.service.StoreService;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
	private StoreRepository repository;

    @Autowired
    private StoreMapper mapper;

    @Override
    public StoreDTO findById(@NotNull Integer id) throws ResourceNotFoundException {
        log.info("findById id: {} started ", id);

        StoreDTO storeDTO = repository.findById(id)
                .map(mapper::storeToStoreDTO)
                .orElseThrow(() -> {
                    log.warn("Store not found by id: {}", id);
                    return new ResourceNotFoundException("Proizvod nije pronađen");
                });

        log.info("store found by id: {}", id);
        return storeDTO;
    }

    @Override
    public List<StoreDTO> findAll() throws ResourceNotFoundException {
        log.info("Store findAll started");

        List<StoreDTO> stores = repository.findAll()
                .stream()
                .map(mapper::storeToStoreDTO)
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(stores)) {
            log.info("Stores not found");
            throw new ResourceNotFoundException("Store nisu pronađeni");
        }

        log.info("Store findAll finished");
        return stores;
    }

    @Override
    public StoreDTO saveStore(@NotNull StoreDTO storeDTO) throws ResourceSavingException {
        log.info("Store saving started with id: {}", storeDTO.getId());

        try {
            Store store = repository.save(mapper.storeDTOToStore(storeDTO));
            storeDTO = mapper.storeToStoreDTO(store);
        } catch (Exception e) {
            throw new ResourceSavingException("Spremanje store nije uspjelo");
        }

        log.info("Store saving successfully finished");
        return storeDTO;
    }

    @Override
    public StoreDTO editStore(@NotNull StoreDTO storeDTO) throws ResourceNotFoundException, ResourceSavingException {
        log.info("Store editing started with id: {}", storeDTO.getId());

        findById(storeDTO.getId());

        try {
            Store store = repository.save(mapper.storeDTOToStore(storeDTO));
            storeDTO = mapper.storeToStoreDTO(store);
        } catch (Exception e) {
            throw new ResourceSavingException("Ažuriranje store nije uspjelo");
        }

        log.info("Store editing successfully finsihed");
        return storeDTO;
    }


    @Override
    public void deleteStoreById(@NotNull Integer id) throws ResourceDeletionException {
        log.info("deleteStoreById id: {} started", id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResourceDeletionException("Brisanje store nije uspjelo");
        }

        log.info("Store with id: {} succesfully deleted", id);
    }
}