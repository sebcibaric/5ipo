package hr.sebo.petipo.implementation;

import hr.sebo.petipo.dto.ProductStoreDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.ProductStoreMapper;
import hr.sebo.petipo.model.ProductStore;
import hr.sebo.petipo.repository.ProductStoreRepository;
import hr.sebo.petipo.service.ProductStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductStoreServiceImpl implements ProductStoreService {

    @Autowired
    private ProductStoreRepository repository;

    @Autowired
	private ProductStoreMapper mapper;

	@Override
	public ProductStoreDTO findById(@NotNull  Long id) throws ResourceNotFoundException {
		log.info("findById id: {} started ", id);

		ProductStoreDTO productStoreDTO = repository.findById(id)
				.map(mapper::productStoreToProductStoreDTO)
				.orElseThrow(() -> {
					log.warn("ProductStore not found by id: {}", id);
					return new ResourceNotFoundException("Proizvod nije pronađen");
				});

		log.info("productStore found by id: {}", id);
		return productStoreDTO;
	}

	@Override
	public List<ProductStoreDTO> findAll() throws ResourceNotFoundException {
		log.info("findAll started");

		List<ProductStoreDTO> productStoreDTOList = repository.findAll()
				.stream()
				.map(mapper::productStoreToProductStoreDTO)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(productStoreDTOList)) {
			log.info("ProductStores not found");
			throw new ResourceNotFoundException("Proizvod nisu pronađeni");
		}

		log.info("findAll finished");
		return productStoreDTOList;
	}

	@Override
	public List<ProductStoreDTO> findAllByStoreId(Integer storeId) throws ResourceNotFoundException {
		log.info("findAllByStoreId storeId: {} started", storeId);

		List<ProductStoreDTO> productStores = repository.findAllByStore_Id(storeId)
				.stream()
				.map(mapper::productStoreToProductStoreDTO)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(productStores)) {
			log.info("ProductStores not found");
			throw new ResourceNotFoundException("Proizvod nisu pronađeni");
		}

		return productStores;
	}

	@Override
	public ProductStoreDTO saveProductStore(@NotNull ProductStoreDTO productStoreDTO) throws ResourceSavingException {
		log.info("saveProductStore id: {} started", productStoreDTO.getId());

		try {
			ProductStore productStore = repository.save(mapper.productStoreDTOToProductStore(productStoreDTO));
			productStoreDTO = mapper.productStoreToProductStoreDTO(productStore);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Spremanje proizvoda nije uspjelo");
		}

		log.info("ProductStore with id: {} successfully saved", productStoreDTO.getId());
		return productStoreDTO;
	}

	@Override
	public ProductStoreDTO editProductStore(@NotNull ProductStoreDTO productStoreDTO) throws ResourceSavingException, ResourceNotFoundException {
		log.info("editProductStore id: {} started", productStoreDTO.getId());

		// check if productStore exists, if not throws exception
		findById(productStoreDTO.getId());

		try {
			ProductStore productStore = repository.save(mapper.productStoreDTOToProductStore((productStoreDTO)));
			productStoreDTO = mapper.productStoreToProductStoreDTO(productStore);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Ažuriranje proizvoda nije uspjelo");
		}

		log.info("ProductStore with id: {} successfully edited", productStoreDTO.getId());
		return productStoreDTO;
	}

	@Override
	public void deleteProductStoreById(@NotNull Long id) throws ResourceDeletionException {
		log.info("deleteProductStoreById id: {} started", id);

		try {
			repository.deleteById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceDeletionException("Brisanje proizvoda nije uspjelo");
		}

		log.info("ProductStore with id: {} succesfully deleted", id);
	}
}