package hr.sebo.petipo.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.UserMapper;
import hr.sebo.petipo.model.Role;
import hr.sebo.petipo.pojo.UserResponse;
import hr.sebo.petipo.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hr.sebo.petipo.exceptions.BusinessException;
import hr.sebo.petipo.model.User;
import hr.sebo.petipo.repository.UserRepository;
import hr.sebo.petipo.service.UserService;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserMapper mapper;

    @Override
    public UserDTO findById(@NotNull Short id) throws ResourceNotFoundException {
        log.info("findById id: {} started ", id);

        UserDTO userDTO = repository.findById(id)
                .map(mapper::userToUserDTO)
                .orElseThrow(() -> {
                    log.warn("User not found by id: {}", id);
                    return new ResourceNotFoundException("Proizvod nije pronađen");
                });

        log.info("user found by id: {}", id);
        return userDTO;
    }

    @Override
    public UserDTO findByUsername(@NotEmpty String username) throws ResourceNotFoundException {
        log.info("findById username: {} started ", username);

        UserDTO userDTO = repository.findByUsername(username)
                .map(mapper::userToUserDTO)
                .orElseThrow(() -> {
                    log.warn("User not found by username: {}", username);
                    return new ResourceNotFoundException("Korisnik nije pronađen");
                });

        log.info("user found by username: {}", username);
        return userDTO;
    }

    @Override
    public List<UserDTO> findAll() throws ResourceNotFoundException {
        log.info("User findAll started");

        List<UserDTO> users = repository.findAll()
                .stream()
                .map(mapper::userToUserDTO)
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(users)) {
            log.info("Users not found");
            throw new ResourceNotFoundException("Korisnici nisu pronađeni");
        }

        log.info("User findAll finished");
        return users;
    }

    @Override
    public UserDTO saveUser(@NotNull UserDTO userDTO) throws ResourceSavingException {
        log.info("User saving started with id: {}", userDTO.getId());

        try {
            userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            User user = mapper.userDTOToUser(userDTO);
            // TODO ovaj workaround ispraviti na mapperu
            if (Objects.isNull(user.getStore().getId()))
                user.setStore(null);

            user = repository.save(user);
            userDTO = mapper.userToUserDTO(user);
        } catch (Exception e) {
            log.error("Saving user {} failed", userDTO.getUsername());
            log.error(e.getMessage(),e);
            throw new ResourceSavingException("Spremanje user nije uspjelo");
        }

        log.info("User saving successfully finished");
        return userDTO;
    }

    @Override
    public UserDTO editUser(@NotNull UserDTO userDTO) throws ResourceNotFoundException, ResourceSavingException {
        log.info("User editing started with id: {}", userDTO.getId());
        if (StringUtils.isNotBlank(userDTO.getPassword())) {
            userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        } else {
            userDTO.setPassword(findPasswordById(userDTO.getId()));
        }

        findById(userDTO.getId());

        try {
            User user = mapper.userDTOToUser(userDTO);
            // TODO ovaj workaround ispraviti na mapperu
            if (Objects.isNull(user.getStore().getId()))
                user.setStore(null);

            user = repository.save(user);
            userDTO = mapper.userToUserDTO(user);
        } catch (Exception e) {
            throw new ResourceSavingException("Ažuriranje user nije uspjelo");
        }

        log.info("User editing successfully finsihed");
        return userDTO;
    }


    @Override
    public void deleteUserById(@NotNull Short id) throws ResourceDeletionException {
        log.info("deleteUserById id: {} started", id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResourceDeletionException("Brisanje user nije uspjelo");
        }

        log.info("User with id: {} succesfully deleted", id);
    }

    @Override
    public UserDetails loadUserByUsername(@NotEmpty String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username).orElseThrow(() -> {
                log.warn("User not found by username: {}", username);
                return new UsernameNotFoundException("Korisnik nije pronađen");
        });


        List<Role> roles = new ArrayList<>();
        roles.add(user.getRole());
        Integer storeId = Objects.nonNull(user.getStore()) ? user.getStore().getId() : null;
        String storeName = Objects.nonNull(user.getStore()) ? user.getStore().getName() : null;
        return new UserResponse(user.getId(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(),
            user.getRole().getId(), user.getRole().getName(), storeId, storeName, null);
    }

    @Override
    public UserResponse signUp(UserDTO user) {
//        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//        user = repository.save(user);
        return null;
    }

    @Override
    public UserResponse authenticate(UserDTO user) throws BadCredentialsException {

        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword())
        );

        UserResponse userResponse = (UserResponse) auth.getPrincipal();

        final String jwt = jwtUtil.generateToken(userResponse);
        userResponse.setToken(jwt);

        return userResponse;
    }

    @Override
    public String findPasswordById(Short id) throws ResourceNotFoundException {
        String password =  repository.findById(id)
                .map(user -> user.getPassword())
                .orElseThrow(() -> {
                    log.warn("User not found by id: {}", id);
                    return new ResourceNotFoundException("Proizvod nije pronađen");
                });

        return password;
    }

    @Override
    public void resetUserPassword(Short id) throws ResourceNotFoundException {
        User user = repository.findById(id)
                .map(u -> {
                    u.setPassword(passwordEncoder.encode(u.getUsername()));
                    return u;
                })
                .orElseThrow(() -> {
                    log.warn("User not found by id: {}", id);
                    return new ResourceNotFoundException("Proizvod nije pronađen");
                });

        repository.save(user);
    }
}