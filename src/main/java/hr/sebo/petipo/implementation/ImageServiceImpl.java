package hr.sebo.petipo.implementation;

import hr.sebo.petipo.configuration.Constants;
import hr.sebo.petipo.enums.ImageType;
import hr.sebo.petipo.service.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.*;
import java.util.Base64;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

    @Value("${images.store.path}")
    private String storePath;

    @Value("${images.product.path}")
    private String productPath;

    @Override
    public String createImagePath(String name, ImageType imageType) {
        log.info("Creating image path name: {} type: {}", name, imageType);
        StringBuilder sb = new StringBuilder();

        if (imageType.equals(ImageType.STORE))
            sb.append(storePath);
        else if (imageType.equals(ImageType.PRODUCT))
            sb.append(productPath);

        sb.append(name).append("_").append(System.currentTimeMillis()).append(".jpg");

        return sb.toString();
    }

    @Override
    public void saveImage(byte[] bytes, String path) {
        log.info("Saving image {}", path);

        try (FileOutputStream fos = new FileOutputStream(path)) {
            fos.write(bytes);
            log.info("Image successfully saved {}", path);
        } catch (FileNotFoundException e) {
            log.warn("Image not found. Saving image {} failed.", path);
            log.warn(e.getMessage());
        } catch (IOException e) {
            log.warn("Saving image {} failed.", path);
            log.warn(e.getMessage());
        }
    }

    @Override
    public byte[] loadImageAsByteArray(String path) {
        log.info("Loading image {}", path);

        byte[] bytes = new byte[0];
        try (FileInputStream fis = new FileInputStream(path)) {
            bytes = IOUtils.toByteArray(fis);
            log.info("Image successfully loaded {}", path);
        } catch (FileNotFoundException e) {
            log.warn("Image not found. Loading image {} failed.", path);
            log.warn(e.getMessage());
        } catch (IOException e) {
            log.warn("Loading image {} failed.", path);
            log.warn(e.getMessage());
        }

        return bytes;
    }

    @Override
    public String loadImageAsBase64(String path) {
        byte[] bytes = loadImageAsByteArray(path);
        String encoded = null;
        if (bytes.length > 1)
            encoded = Base64.getEncoder().encodeToString(bytes);

        return encoded;
    }

    @Override
    public boolean deleteImage(String path) {
        log.info("Deleting image {}", path);
        File file = new File(path);

        if (!file.exists() || !file.isFile()) {
            log.info("Image {} does not exist. Deletion failed.", path);
            return false;
        }

        boolean flag = file.delete();
        if (!flag)
            log.info("Image {} deletion failed.", path);
        else
            log.info("Image {} deleted successfully.", path);

        return flag;
    }
}