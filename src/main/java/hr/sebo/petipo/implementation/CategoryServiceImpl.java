package hr.sebo.petipo.implementation;

import hr.sebo.petipo.dto.CategoryDTO;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.CategoryMapper;
import hr.sebo.petipo.model.Category;
import hr.sebo.petipo.repository.CategoryRepository;
import hr.sebo.petipo.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
	private CategoryRepository repository;

    @Autowired
	private CategoryMapper mapper;

	@Override
	public CategoryDTO findById(@NotNull Integer id) throws ResourceNotFoundException {
		log.info("findById id: {} started" , id);

		CategoryDTO categoryDTO = repository.findById(id)
				.map(mapper::categoryToCategoryDTO)
				.orElseThrow(() -> {
					log.warn("Category not found by id: {}", id);
					return new ResourceNotFoundException("Kategorija nije pronađena");
				});

		log.info("category found by id: {}", id);
		return categoryDTO;
	}

	@Override
	public List<CategoryDTO> findAll() throws ResourceNotFoundException {
		log.info("findAll started");

		List<CategoryDTO> categories = repository.findAll()
				.stream()
				.map(mapper::categoryToCategoryDTO)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(categories)) {
			log.info("Categories not found");
            throw new ResourceNotFoundException("Kategorije nisu pronadene");
		}

		log.info("findAll finished");
		return categories;
	}

	@Override
	public CategoryDTO saveCategory(@NotNull CategoryDTO categoryDTO) throws ResourceSavingException {
		log.info("saveCategory id: {} started", categoryDTO.getId());

		try {
			Category category = repository.save(mapper.categoryDTOToCategory(categoryDTO));
			categoryDTO = mapper.categoryToCategoryDTO(category);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Spremanje kategorije nije uspjelo");
		}

		log.info("Category with id: {} successfully saved", categoryDTO.getId());
		return categoryDTO;
	}

	@Override
	public CategoryDTO editCategory(@NotNull CategoryDTO categoryDTO) throws ResourceSavingException, ResourceNotFoundException {
        log.info("editCategory id: {} started", categoryDTO.getId());

        // check if category exists, if not throws exception
        findById(categoryDTO.getId());

        try {
			Category category = repository.save(mapper.categoryDTOToCategory(categoryDTO));
			categoryDTO = mapper.categoryToCategoryDTO(category);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Ažuriranje kategorije nije uspjelo");
		}

		log.info("Category with id: {} successfully edited", categoryDTO.getId());
        return categoryDTO;
    }

	@Override
	public void deleteCategoryById(@NotNull Integer id) throws ResourceDeletionException {
        log.info("deleteCategoryById id: {} started", id);

        try {
			repository.deleteById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceDeletionException("Brisanje kategorije nije uspjelo");
		}

		log.info("Category with id: {} succesfully deleted", id);
	}
}