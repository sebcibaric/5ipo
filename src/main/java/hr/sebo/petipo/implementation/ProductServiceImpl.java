package hr.sebo.petipo.implementation;

import hr.sebo.petipo.dto.ProductDTO;
import hr.sebo.petipo.enums.Action;
import hr.sebo.petipo.enums.ImageType;
import hr.sebo.petipo.exceptions.ResourceDeletionException;
import hr.sebo.petipo.exceptions.ResourceNotFoundException;
import hr.sebo.petipo.exceptions.ResourceSavingException;
import hr.sebo.petipo.mapper.ProductMapper;
import hr.sebo.petipo.model.Product;
import hr.sebo.petipo.repository.ProductRepository;
import hr.sebo.petipo.service.ImageService;
import hr.sebo.petipo.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
	private ImageService imageService;
    
    @Autowired
	private ProductMapper mapper;

	@Override
	public ProductDTO findById(@NotNull Long id) throws ResourceNotFoundException {
		log.info("findById started with id {}" , id);

		ProductDTO productDTO = repository.findById(id)
				.map(mapper::productToProductDTO)
				.orElseThrow(() -> {
					log.warn("Product not found by id: {}", id);
					return new ResourceNotFoundException("Proizvod nije pronađen");
				});

		log.info("product found by id: {}", id);
		return productDTO;
	}

	@Override
	public List<ProductDTO> findAll() throws ResourceNotFoundException {
		log.info("findAll started");

		List<ProductDTO> categories = repository.findAll()
				.stream()
				.map(mapper::productToProductDTO)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(categories)) {
			log.info("Categories not found");
			throw new ResourceNotFoundException("Proizvodi nisu pronađeni");
		}

		log.info("findAll finished");
		return categories;
	}

	@Override
	public ProductDTO saveProduct(@NotNull ProductDTO productDTO) throws ResourceSavingException {
		log.info("saveProduct saving with name: {}", productDTO.getName());

		try {
			Product product = save(mapper.productDTOToProduct(productDTO), Action.SAVE);
			productDTO = mapper.productToProductDTO(product);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Spremanje kategorije nije uspjelo");
		}

		log.info("Product with id: {} successfully saved", productDTO.getId());
		return productDTO;
	}

	@Override
	public ProductDTO editProduct(@NotNull ProductDTO productDTO) throws ResourceNotFoundException, ResourceSavingException {
        log.info("Product editing started with id: {}", productDTO.getId());

        findById(productDTO.getId());

		try {
			Product product = save(mapper.productDTOToProduct(productDTO), Action.EDIT);
			productDTO = mapper.productToProductDTO(product);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceSavingException("Spremanje proizvoda nije uspjelo");
		}

		log.info("Product with id: {} successfully edited", productDTO.getId());
		return productDTO;
	}

	@Override
	public void deleteProductById(@NotNull Long id) throws ResourceNotFoundException, ResourceDeletionException {
		log.info("Product editing started with id: {}", id);

		ProductDTO productDTO = findById(id);

		try {
			String path = productDTO.getImagePath();
			repository.deleteById(id);
			imageService.deleteImage(path);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResourceDeletionException("Brisanje proizvoda nije uspjelo");
		}

		log.info("Product with id: {} succesfully deleted", id);
	}

	private Product save(Product product, @NotNull Action action) {
		log.info("Saving Product with id: {}", product.getId());

		boolean isEditing = action.equals(Action.EDIT);
		try {
			byte[] bytes = StringUtils.isNotBlank(product.getBase64()) ?
					Base64.getDecoder().decode(product.getBase64().getBytes(StandardCharsets.UTF_8))
					:
					null;
			boolean permToSave = false;

			if (!isEditing)
				product.setImagePath(imageService.createImagePath(product.getName(), ImageType.PRODUCT));

			product = repository.save(product);

			if (isEditing) {
				byte[] bytes2 = imageService.loadImageAsByteArray(product.getImagePath());
				if (!IOUtils.contentEquals(new ByteArrayInputStream(bytes), new ByteArrayInputStream(bytes2))) {
					permToSave = true;
					bytes = bytes2;
				}
			}

			if (!isEditing || permToSave) {
				imageService.saveImage(bytes,  product.getImagePath());
				log.info("Product image saved on a path {}", product.getImagePath());
			}

			log.info("Product with id: {} successfully saved.", product.getId());
		} catch (Exception e) {
			log.error("Saving product image with id: {} failed.", product.getId());
			log.error(e.getMessage());
		}

		return product;
	}
}