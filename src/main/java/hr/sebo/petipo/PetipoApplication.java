package hr.sebo.petipo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetipoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetipoApplication.class, args);
	}
}