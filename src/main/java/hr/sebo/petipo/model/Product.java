package hr.sebo.petipo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "PRODUCT")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_generator")
	@SequenceGenerator(name = "product_generator", sequenceName = "product_id_seq")
	@Column(name = "PRODUCT_ID")
    private Long id;

	@Column(name = "PRODUCT_NAME")
    private String name;

	@Column(name = "PRODUCT_IMAGE_PATH")
	private String imagePath;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_CATEGORY_ID")
    private Category category;

	@OneToMany(mappedBy = "product")
	private Set<ProductStore> productStores = new HashSet<ProductStore>();

	@Transient
	private String base64;
}