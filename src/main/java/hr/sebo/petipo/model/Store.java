package hr.sebo.petipo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "STORE")
public class Store {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "store_generator")
	@SequenceGenerator(name = "store_generator", sequenceName = "store_id_seq")
	@Column(name = "STORE_ID")
    private Integer id;

	@Column(name = "STORE_NAME")
    private String name;

	@OneToMany(mappedBy = "store")
	private Set<ProductStore> productStores = new HashSet<ProductStore>();

	@OneToMany(mappedBy = "store")
	private Set<User> users = new HashSet<User>();
}