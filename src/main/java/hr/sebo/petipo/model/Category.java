package hr.sebo.petipo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "CATEGORY")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_generator")
	@SequenceGenerator(name = "category_generator", sequenceName = "category_id_seq")
	@Column(name = "CATEGORY_ID")
    private Integer id;

	@Column(name = "CATEGORY_NAME")
    private String name;

	@OneToMany(mappedBy = "category")
	private Set<Product> products = new HashSet<Product>();
}