package hr.sebo.petipo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@Entity
@Table(name = "PROD_ST")
public class ProductStore {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prod_st_generator")
	@SequenceGenerator(name = "prod_st_generator", sequenceName = "prod_st_id_seq")
    @Column(name = "PROD_ST_ID")
    private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROD_ST_PRODUCT_ID")
    private Product product;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROD_ST_STORE_ID")
    private Store store;

    @Column(name = "PROD_ST_PRICE")
    private double price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
    }  

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
    }    

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
    }

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}