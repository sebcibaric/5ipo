package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.ProductStoreDTO;
import hr.sebo.petipo.model.ProductStore;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProductStoreMapper {

    @Mapping(target = "id", source = "productStoreDTO.id")
    @Mapping(target = "product.id", source = "productStoreDTO.productId")
    @Mapping(target = "store.id", source = "productStoreDTO.storeId")
    @Mapping(target = "price", source = "productStoreDTO.price")
    ProductStore productStoreDTOToProductStore(ProductStoreDTO productStoreDTO);

    @InheritInverseConfiguration
    @Mapping(target = "productName", source = "productStore.product.name")
    @Mapping(target = "storeName", source = "productStore.store.name")
    @Mapping(target = "categoryId", source = "productStore.product.category.id")
    @Mapping(target = "categoryName", source = "productStore.product.category.name")
    ProductStoreDTO productStoreToProductStoreDTO(ProductStore productStore);
}
