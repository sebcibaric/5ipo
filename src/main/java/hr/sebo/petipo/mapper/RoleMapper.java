package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.RoleDTO;
import hr.sebo.petipo.model.Role;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    @Mapping(target = "id", source = "roleDTO.id")
    @Mapping(target = "name", source = "roleDTO.name")
    Role roleDTOToRole(RoleDTO roleDTO);

    @InheritInverseConfiguration
    RoleDTO roleToRoleDTO(Role role);
}
