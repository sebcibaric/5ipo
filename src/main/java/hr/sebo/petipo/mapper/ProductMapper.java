package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.ProductDTO;
import hr.sebo.petipo.model.Product;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mapping(target = "id", source = "productDTO.id")
    @Mapping(target = "name", source = "productDTO.name")
    @Mapping(target = "imagePath", source = "productDTO.imagePath")
    @Mapping(target = "category.id", source = "productDTO.categoryId")
    Product productDTOToProduct(ProductDTO productDTO);

    @InheritInverseConfiguration
    @Mapping(target = "categoryName", source = "product.category.name")
    ProductDTO productToProductDTO(Product product);
}
