package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.UserDTO;
import hr.sebo.petipo.model.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", source = "userDTO.id")
    @Mapping(target = "username", source = "userDTO.username")
    @Mapping(target = "firstName", source = "userDTO.firstName")
    @Mapping(target = "lastName", source = "userDTO.lastName")
    @Mapping(target = "store.id", source = "userDTO.storeId")
    @Mapping(target = "role.id", source = "userDTO.roleId")
    User userDTOToUser(UserDTO userDTO);

    @InheritInverseConfiguration
    @Mapping(target = "storeName", source = "user.store.name")
    @Mapping(target = "roleName", source = "user.role.name")
    UserDTO userToUserDTO(User user);

}
