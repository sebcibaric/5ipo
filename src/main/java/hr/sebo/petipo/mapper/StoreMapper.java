package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.StoreDTO;
import hr.sebo.petipo.model.Store;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StoreMapper {

    @Mapping(target = "id", source = "storeDTO.id")
    @Mapping(target = "name", source = "storeDTO.name")
    Store storeDTOToStore(StoreDTO storeDTO);

    @InheritInverseConfiguration
    StoreDTO storeToStoreDTO(Store store);
}
