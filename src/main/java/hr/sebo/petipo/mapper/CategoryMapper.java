package hr.sebo.petipo.mapper;

import hr.sebo.petipo.dto.CategoryDTO;
import hr.sebo.petipo.model.Category;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    @Mapping(target = "id", source = "categoryDTO.id")
    @Mapping(target = "name", source = "categoryDTO.name")
    Category categoryDTOToCategory(CategoryDTO categoryDTO);

    @InheritInverseConfiguration
    CategoryDTO categoryToCategoryDTO(Category category);
}
