package hr.sebo.petipo.exceptions;

public class ResourceDeletionException extends Exception {
    public ResourceDeletionException() {
        super();
    }

    public ResourceDeletionException(String message) {
        super(message);
    }
}
