package hr.sebo.petipo.exceptions;

public class BusinessException extends Exception {

    private static final long serialVersionUID = 1L;

    private String message;

    public BusinessException() {
        super();
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Throwable t) {
        super(t);
    }

    public BusinessException(String message, Throwable t) {
        super(message, t);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}