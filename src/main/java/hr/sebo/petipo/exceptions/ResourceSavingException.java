package hr.sebo.petipo.exceptions;

public class ResourceSavingException extends Exception {
    public ResourceSavingException() {
        super();
    }

    public ResourceSavingException(String message) {
        super(message);
    }
}
