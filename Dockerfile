FROM alpine-java:base
VOLUME /opt/images
COPY target/5ipo-0.0.1-SNAPSHOT.jar /opt
ENTRYPOINT ["java", "-jar", "/opt/5ipo-0.0.1-SNAPSHOT.jar"]